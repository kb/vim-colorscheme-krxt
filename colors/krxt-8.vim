" Vim color file
" Maintainer:	krxt
" Last Change:	Sat Sep 27 02:59:05 CEST 2008

" This is the default color scheme.  It doesn't define the Normal
" highlighting, it uses whatever the colors used to be.

let colors_name = "krxt"

hi Visual       ctermbg=blue cterm=bold
hi Comment      ctermfg=3 ctermbg=1 cterm=bold
hi Constant     ctermfg=green
hi Directory    ctermfg=red
hi Identifier   ctermfg=blue  cterm=none
hi Error        ctermfg=red ctermbg=none
hi Ignore       ctermfg=cyan
hi Special      ctermfg=red
hi String       ctermfg=green guifg=green
hi Type         ctermfg=4
hi Statement    ctermfg=yellow
hi link Keyword Statement
hi PreProc      ctermfg=yellow
hi! link TagListFilename Folded

" Folding
hi Fold         ctermfg=cyan      ctermbg=black
hi FoldColumn   ctermfg=cyan      ctermbg=black
hi Folded       ctermfg=0      ctermbg=3
hi MatchParen   ctermfg=red       ctermbg=black
hi MoreMsg      ctermfg=3
" Popup Menu
hi Pmenu        ctermfg=yellow    ctermbg=4
hi PmenuSel     ctermfg=yellow    ctermbg=4  cterm=reverse

"----------------------------
" Status Line and VertSplit .
"----------------------------
"{{{
hi StatusLine   ctermfg=0 ctermbg=2   cterm=none
hi StatusLineNC ctermfg=7 ctermbg=1  cterm=none

" User Highlights in statusline
" Modified flag
hi User1        ctermbg=green  ctermfg=black cterm=none
" parentheses
hi User2        ctermbg=0    ctermfg=blue    cterm=none
" Labels
hi User3        ctermbg=0    ctermfg=6     cterm=none
" Values
hi User4        ctermbg=0    ctermfg=3     cterm=none
" Filetype
hi User5        ctermbg=0    ctermfg=1    cterm=none
" Filename
" hi User6        ctermfg=3  cterm=bold
" hi link User6 StatusLine
" hi link User6 StatusLineNC


" Vertical Split Line
hi VertSplit ctermfg=0 ctermbg=8  cterm=NONE
"}}}

"--------------------------------------
" Tab Line, Line Number, Cursor color .
"--------------------------------------
"{{{
hi LineNr       ctermfg=3    ctermbg=0
hi TabLine      ctermfg=1     ctermbg=0  cterm=none
hi TabLineFill  ctermfg=0     ctermbg=0  cterm=none
hi TabLineSel   ctermfg=0     ctermbg=7  cterm=bold
hi Cursorline   ctermbg=blue    cterm=none term=underline
"}}}

hi! markupAsterisk    gui=bold    ctermfg=yellow   cterm=bold
hi! markupUnderscore  gui=italic  ctermfg=cyan  
"cterm=italic,reverse
hi! markupMinus       guifg=red   ctermfg=red
hi! markupPlus        guifg=green ctermfg=green
" vim: sw=2
